const usb = require("usb")

const devices = usb.getDeviceList()
debugger

const myKeyboard = usb.getDeviceList().find(d => d.deviceDescriptor.idProduct === 720)

myKeyboard.open()
myKeyboard.interface(0).claim()

/**
 * @type {import("usb").InEndpoint}
 */
const endpoint = myKeyboard.interface(0).endpoints[0]

endpoint.startPoll()

let counter = 0

endpoint.on("data", data => {
  console.log(data)
  counter++

  if (counter > 5) {
    endpoint.stopPoll()
    endpoint.once("end", () => {
      myKeyboard.close()

      setTimeout(() => {
        process.exit()
      }, 500)
    })
  }
})
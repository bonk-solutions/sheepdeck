import { invoke, off, on, ListenerFunction } from "@renderer/ipc"
import { useEffect, useState } from "react"
import { DeviceDescriptor } from "usb"
import { MsgLookup } from "../../ipc-messages"

/**
 * Use Device Descriptors
 */
export const useDDs = () => {
  const [dds, setDDs] = useState<DeviceDescriptor[]>([])

  useEffect(() => {
    invoke("get-devices", undefined).then(setDDs)

    const handler: ListenerFunction<DeviceDescriptor[]> = (_, dds) => {
      setDDs(dds)
    }

    on("devices-update", handler)

    return () => off("devices-update", handler)
  }, [])

  return dds
}

export const useSignals = (
  onData: (payload: MsgLookup["main"]["usb-data"]) => void,
  onError: (payload: MsgLookup["main"]["usb-error"]) => void
) => {
  useEffect(() => {
    const dataHandler: ListenerFunction<MsgLookup["main"]["usb-data"]> = (_, payload) => onData(payload)
    const errorHandler: ListenerFunction<MsgLookup["main"]["usb-error"]> = (_, payload) => onError(payload)

    on("usb-data", dataHandler)
    on("usb-error", errorHandler)

    return () => {
      off("usb-data", dataHandler)
      off("usb-error", errorHandler)
    }
  }, [])
}
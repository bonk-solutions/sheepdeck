import { useState } from "react"

export const useLocalStorage = <T>(key: string, initialValue: T) => {
  const [value, setValueInner] = useState<T>(() => {
    const item = localStorage.getItem(key)
    return item === null ? initialValue : JSON.parse(item)
  })
  
  const setValueOuter = (newValue: T) => {
    setValueInner(newValue)
    localStorage.setItem(key, JSON.stringify(newValue))
  }

  return [value, setValueOuter] as const
}
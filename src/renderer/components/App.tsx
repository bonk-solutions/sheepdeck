import React, { useState } from "react"

import Tabs from "react-bootstrap/Tabs"
import Tab, { TabProps } from "react-bootstrap/Tab"
import Container from "react-bootstrap/Container"

import { Welcome } from "./Welcome"
import { Keys } from "./tabs/Keys"
import { DeviceOverview } from "./tabs/DeviceOverview"
import { Actions } from "./tabs/Actions"
import { Settings } from "./tabs/Settings"
import { Mappings } from "./tabs/Mappings"
import { Header } from "./Header"

const TabWithContainer = ({ children, ...props }: TabProps) => (
  <Tab {...props}>
    <Container fluid className="flex-grow-1 d-flex flex-column" style={{
      overflowY: "hidden"
    }}>
      {children}
    </Container>
  </Tab>
)

export const App = () => {
  const [showWelcome, setShowWelcome] = useState(true)

  if (showWelcome) {
    return <Welcome hide={() => setShowWelcome(false)}/>
  }

  return (
    <>
      <Header/>
      <Tabs fill defaultActiveKey="devices">
        <TabWithContainer eventKey="devices" title="Geräte">
          <DeviceOverview />
        </TabWithContainer>
        <TabWithContainer eventKey="keys" title="Keys">
          <Keys />
        </TabWithContainer>
        <TabWithContainer eventKey="actions" title="Actions">
          <Actions />
        </TabWithContainer>
        <TabWithContainer eventKey="mappings" title="Mappings">
          <Mappings />
        </TabWithContainer>
        <TabWithContainer eventKey="settings" title="Settings">
          <Settings />
        </TabWithContainer>
      </Tabs>
    </>
  )
}
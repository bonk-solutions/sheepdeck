import React from "react"

import Sheep from "../../assets/sheep.png"
import BonkSolutionsFull from "../../assets/Full.png"

export const Header = () => (
  <div
    className="
      d-flex justify-content-between align-items-center
      header px-4
    "
  >
    <img
      src={Sheep}
      style={{ height: 50 }}
    />
    <h4>SheepDeck</h4>
    <img
      src={BonkSolutionsFull}
      style={{ height: 50 }}
    />
  </div>
)
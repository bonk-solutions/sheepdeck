import React, { ChangeEvent, Dispatch, FormEvent, useEffect, useState, SetStateAction } from "react"
import Col from "react-bootstrap/Col"
import Row from "react-bootstrap/Row"
import Button from "react-bootstrap/Button"
import Form from "react-bootstrap/Form"
import Modal from "react-bootstrap/esm/Modal"

interface MockMapping {
  id: number;
  key: string;
  action: string;
}

interface Props {
  show: boolean
  hide: () => void
  setMockMapping: Dispatch<SetStateAction<MockMapping[]>>
}

export const AddModal = ({ show, hide, setMockMapping }: Props) => {
  const [changeValue, setChangeValue] = useState("")
  const [changeValue2, setChangeValue2] = useState("")

  useEffect(() => {
    if (show) {
      setChangeValue("Key 1")
      setChangeValue2("Action 1")
    }
  }, [show])

  const handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
    setChangeValue(event.target.value)
  }

  const handleChange2 = (event: ChangeEvent<HTMLSelectElement>) => {
    setChangeValue2(event.target.value)
  }

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault()
    const newMapping = {
      id: 0,
      key: changeValue,
      action: changeValue2
    }

    setMockMapping(oldMapping => [...oldMapping, newMapping])
    hide()
  }

  return (
    <Modal centered show={show} onHide={hide}>
      <Modal.Header>
        <Modal.Title>Create Mapping</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Row sm={10}>
            <Form.Label>Key</Form.Label>
            <Form.Control as="select" onChange={handleChange}>
              <option key="1">Key 1</option>
              <option key="2">Key 2</option>
              <option key="3">Key 3</option>
            </Form.Control>
          </Row>
          <Row sm={10}>
            <Form.Label>Action</Form.Label>
            <Form.Control as="select" onChange={handleChange2}>
              <option key="1">Action 1</option>
              <option key="2">Action 2</option>
              <option key="3">Action 3</option>
            </Form.Control>
          </Row>
          <Row>
            <Col>
              <Button
                onClick={hide}
                variant="secondary"
              >
                Cancel
              </Button>
            </Col>
            <Col className="d-flex justify-content-end">
              <Button
                variant="primary"
                type="submit"
              >
                Submit
              </Button>
            </Col>
          </Row>

        </Form>
      </Modal.Body>
    </Modal>
  )
}
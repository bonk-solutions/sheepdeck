import React, { ChangeEvent, Dispatch, FormEvent, useEffect, useState, SetStateAction } from "react"
import Col from "react-bootstrap/Col"
import Row from "react-bootstrap/Row"
import Button from "react-bootstrap/Button"
import Form from "react-bootstrap/Form"
import Modal from "react-bootstrap/esm/Modal"

interface MockMapping {
  id: number;
  key: string;
  action: string;
}

interface MockMappingArray extends Array<any> {
  [index: number]: {
    id: number;
    key: string;
    action: string;
  }
}
interface Props {
  show: boolean
  hide: () => void
  setMockMapping: Dispatch<SetStateAction<MockMapping[]>>
  mockMapping: MockMappingArray
  selectedRow: number
}

export const EditModal = ({ show, hide, setMockMapping, mockMapping, selectedRow }: Props) => {
  const [changeValue, setChangeValue] = useState("")
  const [changeValue2, setChangeValue2] = useState("")

  useEffect(() => {
    if (show) {
      setChangeValue(mockMapping[selectedRow].key)
      setChangeValue2(mockMapping[selectedRow].action)
    }
  }, [show])

  const handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
    setChangeValue(event.target.value)
  }

  const handleChange2 = (event: ChangeEvent<HTMLSelectElement>) => {
    setChangeValue2(event.target.value)
  }

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault()

    setMockMapping(mockMapping.map(item => {
      if (item.id !== selectedRow) return item
      return { ...item, key: changeValue,action: changeValue2 }
    }))

    hide()
  }

  return (
    <Modal centered show={show} onHide={hide}>
      <Modal.Header>
        <Modal.Title>Edit Mapping</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Row sm={10}>
            <Form.Label>Key</Form.Label>
            <Form.Control as="select" onChange={handleChange} defaultValue={mockMapping[selectedRow].key}>
              <option key="1">Key 1</option>
              <option key="2">Key 2</option>
              <option key="3">Key 3</option>
            </Form.Control>
          </Row>
          <Row sm={10}>
            <Form.Label>Action</Form.Label>
            <Form.Control as="select" onChange={handleChange2} defaultValue={mockMapping[selectedRow].action}>
              <option key="1">Action 1</option>
              <option key="2">Action 2</option>
              <option key="3">Action 3</option>
            </Form.Control>
          </Row>
          <Row>
            <Col>
              <Button
                onClick={hide}
                variant="secondary"
              >
                Cancel
              </Button>
            </Col>
            <Col className="d-flex justify-content-end">
              <Button
                variant="primary"
                type="submit"
              >
                Submit
              </Button>
            </Col>
          </Row>

        </Form>
      </Modal.Body>
    </Modal>
  )
}
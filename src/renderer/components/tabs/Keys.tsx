import React, { useState } from "react"

import Col from "react-bootstrap/Col"
import Row from "react-bootstrap/Row"
import Button from "react-bootstrap/Button"
import Form from "react-bootstrap/Form"
import Table from "react-bootstrap/Table"

import GetAppIcon from "@material-ui/icons/GetApp"
import PublishIcon from "@material-ui/icons/Publish"
import CreateIcon from "@material-ui/icons/Create"
import ClearIcon from "@material-ui/icons/Clear"
import AddIcon from "@material-ui/icons/Add"

import { AddModal } from "./keys-components/AddModal"
import { EditModal } from "./keys-components/EditModal"

export const Keys = () => {
  const [showAddModal, setShowAddModal] = useState(false)
  const [showEditModal, setShowEditModal] = useState(false)
  const [selectedRow, setSelectedRow] = useState(0)

  const [mockMapping, setMockMapping] = useState([
    {
      id: 0,
      raw: "#######",
      name: "Sample-Key"
    },
    {
      id: 1,
      raw: "#######",
      name: "Sample-Key"
    },
    {
      id: 2,
      raw: "#######",
      name: "Sample-Key"
    },
    {
      id: 3,
      raw: "#######",
      name: "Sample-Key"
    },
    {
      id: 4,
      raw: "#######",
      name: "Sample-Key"
    },
    {
      id: 5,
      raw: "#######",
      name: "Sample-Key"
    },
  ])

  const [mockData] = useState([
    {
      id: 0,
      name: "Keyboard 0",
      vid: 123,
      pid: 345,
      status: "status"
    },
    {
      id: 1,
      name: "Keyboard 1",
      vid: 123,
      pid: 345,
      status: "status"
    },
    {
      id: 2,
      name: "Keyboard 2",
      vid: 123,
      pid: 345,
      status: "status"
    },
    {
      id: 3,
      name: "Keyboard 3",
      vid: 123,
      pid: 345,
      status: "status"
    },
    {
      id: 4,
      name: "Keyboard 4",
      vid: 123,
      pid: 345,
      status: "status"
    }
  ])

  const handleRemoveItem = (id: number) => {
    setMockMapping(mockMapping.filter(item => item.id !== id))
  }

  const handleEditItem = (id: number) => {
    setSelectedRow(id)
    setShowEditModal(true)
  }

  return (
    <>
      <Row>
        <Col xs={12} className={"text-center"}>
          <Form>
            <Form.Row className={"text-center justify-content-center"}>
              <Col xs={4}>
                <Form.Group controlId="example.SelectCustom">
                  <Form.Label><strong>Verfügbare Geräte:</strong></Form.Label>
                  <Form.Control as="select">
                    {
                      mockData.map((item) => (
                        <option key={item.id}>{item.name}</option>
                      ))
                    }
                  </Form.Control>
                </Form.Group>
              </Col>
            </Form.Row>
          </Form>
        </Col>
      </Row>
      <Row className="justify-content-center m-3">
        <AddModal
          show={showAddModal}
          hide={() => setShowAddModal(false)}
          setMockMapping={setMockMapping}
        />
        <Button variant="primary" className="btn-round mr-3 ml-3" onClick={() => setShowAddModal(true)}><AddIcon /></Button>
        <Button variant="primary" className="btn-round mr-3 ml-3"><PublishIcon /></Button>
        <Button variant="primary" className="btn-round mr-3 ml-3"><GetAppIcon /></Button>
      </Row>
      <Row className="justify-content-center ml-5 mr-5 mt-5">
        <Table striped bordered className="text-center table-hover">
          <thead>
            <tr>
              <th className="align-middle w-25">Id</th>
              <th className="align-middle w-25">Raw Input</th>
              <th className="align-middle w-25">Name</th>
              <th className="align-middle w-25">Actions</th>
            </tr>
          </thead>
          <tbody>
            {
              mockMapping.map((item) => (
                <tr key={item.id}>
                  <td className="align-middle">{item.id}</td>
                  <td className="align-middle">{item.raw}</td>
                  <td className="align-middle">{item.name}</td>
                  <td className="align-middle">
                    <EditModal
                      show={showEditModal}
                      hide={() => setShowEditModal(false)}
                      setMockMapping={setMockMapping}
                      mockMapping={mockMapping}
                      selectedRow={selectedRow}
                    />
                    <Button className="btn-round mr-3 ml-3" onClick={() => handleEditItem(item.id)}><CreateIcon /></Button>
                    <Button className="btn-round mr-3 ml-3" onClick={() => handleRemoveItem(item.id)}><ClearIcon /></Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>
      </Row>
    </>
  )
}
import * as React from "react"
import { Device } from "usb"


interface Props {
  device: Device
  name: string
  setName: (name: string, vendorId: number, productId: number) => void
}

export const DeviceEntry = ({ device, name, setName }: Props) => {
  function handleNameChange(event: React.ChangeEvent<HTMLInputElement>) {
    setName(
      event.target.value,
      device.deviceDescriptor.idVendor,
      device.deviceDescriptor.idProduct
    )
  }

  return (
    <p key={device.deviceDescriptor.idProduct}>
      <input
        value={name}
        onChange={handleNameChange}
        placeholder="Keyboard name"
      />
      idProduct: {device.deviceDescriptor.idProduct}
      {" "}
      idVendor: {device.deviceDescriptor.idVendor}
      {/* <button onClick={startListen}>Listen</button>
      <button onClick={stopListen}>Stop</button> */}
    </p>
  )
}
import React, { useState } from "react"

import Row from "react-bootstrap/Row"

import { FoundKeyboards } from "../FoundKeyboards"
import { ChangeNameModal } from "./device-components/ChangeNameModal"
import { DeviceTable } from "./device-components/Table"

export const DeviceOverview = () => {
  const [showModal] = useState(false)

  return (
    <>
      <Row className="justify-content-center mt-3">
        <p>
          <FoundKeyboards/>
        </p>
      </Row>

      <Row style={{
        overflowY: "scroll"
      }}>
        <DeviceTable/>
      </Row>

      <ChangeNameModal show={showModal}/>
    </>
  )
}
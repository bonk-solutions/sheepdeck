import React from "react"

import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Container from "react-bootstrap/esm/Container"
import Form from "react-bootstrap/esm/Form"

import TwitterIcon from "@material-ui/icons/Twitter"
import YouTubeIcon from "@material-ui/icons/YouTube"

export const Settings = () => {

  return (
    <Container fluid>
      <Row className="mb-0 justify-content-center">
        <h3>API Settings</h3>
      </Row>
      <Row className="m-0">
        <hr className="w-100 ml-4 mr-4" style={{ borderColor: "#FFFFFF" }} />
      </Row>

      <Row>
        <h4><TwitterIcon fontSize="large" className="mb-2 mr-3" /> Twitter</h4>
      </Row>
      <Form as={Row} className="w-50">
        <Form.Label column sm="2">API Call</Form.Label>
        <Col sm="10" className="mb-3">
          <Form.Control
            type="text"
            placeholder="Input"
          />
        </Col>
        <Form.Label column sm="2">API Call</Form.Label>
        <Col sm="10" className="mb-3">
          <Form.Control
            type="text"
            placeholder="Input"
          />
        </Col>
        <Form.Label column sm="2">API Call</Form.Label>
        <Col sm="10" className="mb-3">
          <Form.Control
            type="text"
            placeholder="Input"
          />
        </Col>
      </Form>

      <Row>
        <h4><YouTubeIcon fontSize="large" className="mb-2 mr-3" />Youtube</h4>
      </Row>
      <Form as={Row} className="w-50">
        <Form.Label column sm="2">API Call</Form.Label>
        <Col sm="10" className="mb-3">
          <Form.Control
            type="text"
            placeholder="Input"
          />
        </Col>
        <Form.Label column sm="2">API Call</Form.Label>
        <Col sm="10" className="mb-3">
          <Form.Control
            type="text"
            placeholder="Input"
          />
        </Col>
        <Form.Label column sm="2">API Call</Form.Label>
        <Col sm="10" className="mb-3">
          <Form.Control
            type="text"
            placeholder="Input"
          />
        </Col>
      </Form>
    </Container>
  )
}

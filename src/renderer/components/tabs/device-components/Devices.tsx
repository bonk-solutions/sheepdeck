import React, { useState } from "react"
import Button from "react-bootstrap/Button"
import Alert from "react-bootstrap/Alert"

import Container from "react-bootstrap/Container"
import Table from "react-bootstrap/Table"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import { useDDs, useSignals } from "@renderer/hooks/use-devices"
import { DeviceDescriptor } from "usb"
import { invoke } from "@renderer/ipc"
import { filterDDOut, findDDIn, getKeyFromDD } from "@renderer/functions"

// const findKeyboard = (
//   keyboards: Keyboard[],
//   vendorId: number,
//   productId: number
// ) => {
//   return keyboards.find(k => (
//     k.vendorId === vendorId
//     && k.productId === productId
//   ))
// }

export const Devices = () => {
  // const [keyboards, setKeyboards] = useLocalStorage<Keyboard[]>("keyboards", [])
  
  // const [error, setError] = React.useState()

  // const [outputWindowData, setOutputWindowData] = React.useState("")

  // function changeName(name: string, vendorId: number, productId: number) {
  //   const keyboard = findKeyboard(keyboards, vendorId, productId)

  //   if (!keyboard) {
  //     const newKeyboard: Keyboard = { name, vendorId, productId }
  //     setKeyboards([...keyboards, newKeyboard])
  //     return
  //   }

  //   if (name === "") { // entfernen
  //     setKeyboards(keyboards.filter(k => !(k.vendorId === vendorId && k.productId === productId)))
  //     return
  //   }

  //   keyboard.name = name

  //   setKeyboards(keyboards.map(k => {
  //     if (k.vendorId === vendorId && k.productId === productId) {
  //       return keyboard!
  //     }

  //     return k
  //   }))
  // }

  // function errorHandler(error: Error) {
  //   if (error) {
  //     throw error
  //   }
  // }

  // function dataHandler(data: Buffer) {
  //   if (!data) {
  //     throw new Error("Buffer is undefined")
  //   }

  //   console.log("handling", data)

  //   const actualData = data.toJSON().data

  //   const signalIsZeros = actualData.every(byte => byte === 0)

  //   if (lastSignalWasZeros && signalIsZeros) {
  //     return
  //   }

  //   lastSignalWasZeros = signalIsZeros

  //   console.log(actualData)
  //   setOutputWindowData(JSON.stringify(actualData))
  // }

  const error = null
  const dds = useDDs()

  const [actives, setActives] = useState<DeviceDescriptor[]>([])

  const handleToggleActiveDevice = (dd: DeviceDescriptor) => {
    if (actives.find(findDDIn(dd))) {
      invoke("deactivate-device", dd)
      setActives(actives.filter(filterDDOut(dd)))
    } else {
      invoke("activate-device", dd)
      setActives([...actives, dd])
    }
  }

  const [log, setLog] = useState<string[]>([])

  useSignals(
    ({ data }) => {
      setLog(log => [...log.slice(-9), data.toString()])
    },
    console.error
  )

  return (
    <Container fluid>
      {error && (
        <Row>
          <Col>
            <Alert variant="danger">{JSON.stringify(error)}</Alert>
          </Col>
        </Row>
      )}

      <Row>
        <Col>
          <Table striped>
            <thead>
              <tr>
                <th>Vendor ID</th>
                <th>Produkt ID</th>
                <th>Aktionen</th>
              </tr>
            </thead>
            <tbody>
              {dds.map(dd => (
                <tr key={getKeyFromDD(dd)}>
                  <td>{dd.idVendor}</td>
                  <td>{dd.idProduct}</td>
                  <td>
                    <Button
                      size="sm"
                      className="ml-2"
                      onClick={() => handleToggleActiveDevice(dd)}
                    >
                      {actives.find(findDDIn(dd)) ? "Deactivate" : "Activate"}
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
        <Col className="p-3 d-flex flex-column">
          <Row>
            <h5>Rohe Signale</h5>
          </Row>
          <Row className="flex-grow-1">
            <textarea
              readOnly
              disabled
              className="h-100 w-100"
              style={{
                resize: "none"
              }}
              value={log.join("\n") || "Warte auf Daten..."}
            >
            </textarea>
          </Row>
        </Col>
      </Row>
    </Container>
  )
}

import React from "react"
import Col from "react-bootstrap/Col"
import Row from "react-bootstrap/Row"
import Button from "react-bootstrap/Button"
import "bootswatch/dist/cyborg/bootstrap.min.css"
import Modal from "react-bootstrap/esm/Modal"
import Form from "react-bootstrap/esm/Form"

export const ChangeNameModal = ({ show }: { show: boolean }) => (
  <Modal centered show={show}>
    <Modal.Header>
      <Modal.Title>Change Keyboard Name</Modal.Title>
    </Modal.Header>

    <Modal.Body>
      <Form>
        <Row sm={10}>
          {/* <Form.Control className="ml-3 mr-3" type="text" defaultValue={mockData[selectedRow].name} /> */}
        </Row>
        <Row>
          <Col>
            <Button variant="secondary">Cancel</Button>
          </Col>
          <Col className="d-flex justify-content-end">
            <Button variant="primary" type="submit">Submit</Button>
          </Col>
        </Row>

      </Form>
    </Modal.Body>
  </Modal>
)
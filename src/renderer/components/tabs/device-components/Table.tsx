import React from "react"
import Button from "react-bootstrap/Button"
import Badge from "react-bootstrap/Badge"
import Table from "react-bootstrap/Table"
import CreateIcon from "@material-ui/icons/Create"
import { getKeyFromDD } from "@renderer/functions"
import { useDDs } from "@renderer/hooks/use-devices"

export const DeviceTable = () => {
  const dds = useDDs()

  return (
    <Table striped bordered className="text-center">
      <thead>
        <tr>
          <th className="align-middle">Vendor ID</th>
          <th className="align-middle">Product ID</th>
          <th className="align-middle">Status</th>
          <th className="align-middle">Actions</th>
        </tr>
      </thead>
      <tbody>
        {
          dds.map((dd) => (
            <tr key={getKeyFromDD(dd)}>
              <td className="align-middle">
                {dd.idVendor}
              </td>
              <td className="align-middle">
                {dd.idProduct}
              </td>
              <td className="align-middle">
                <Badge pill variant="danger">
                  status
                </Badge>
              </td>
              <td className="align-middle">
                <Button className="btn-round">
                  <CreateIcon />
                </Button>
              </td>
            </tr>
          ))}
      </tbody>
    </Table>
  )
}
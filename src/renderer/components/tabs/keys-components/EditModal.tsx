import React, { ChangeEvent, Dispatch, FormEvent, useEffect, useState, SetStateAction } from "react"
import Col from "react-bootstrap/Col"
import Row from "react-bootstrap/Row"
import Button from "react-bootstrap/Button"
import Form from "react-bootstrap/Form"
import Modal from "react-bootstrap/esm/Modal"

interface MockMapping {
  id: number;
  raw: string;
  name: string;
}

interface MockMappingArray extends Array<any> {
  [index: number]: {
    id: number;
    raw: string;
    name: string;
  }
}
interface Props {
  show: boolean
  hide: () => void
  setMockMapping: Dispatch<SetStateAction<MockMapping[]>>
  mockMapping: MockMappingArray
  selectedRow: number
}

export const EditModal = ({ show, hide, setMockMapping, mockMapping, selectedRow }: Props) => {
  const [changeValue, setChangeValue] = useState("")

  useEffect(() => {
    if (show) {
      setChangeValue(mockMapping[selectedRow].name)
    }
  }, [show])

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setChangeValue(event.target.value)
  }

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault()

    setMockMapping(mockMapping.map(item => {
      if (item.id !== selectedRow) return item
      return { ...item, name: changeValue }
    }))

    hide()
  }

  return (
    <Modal centered show={show} onHide={hide}>
      <Modal.Header>
        <Modal.Title>Edit Mapping</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Row sm={10}>
            <Form.Control
              className="ml-3 mr-3"
              onChange={handleChange}
              type="text"
              defaultValue={mockMapping[selectedRow].name}
            />
          </Row>
          <Row>
            <Col>
              <Button
                onClick={hide}
                variant="secondary"
              >
                Cancel
              </Button>
            </Col>
            <Col className="d-flex justify-content-end">
              <Button
                variant="primary"
                type="submit"
              >
                Submit
              </Button>
            </Col>
          </Row>

        </Form>
      </Modal.Body>
    </Modal>
  )
}
import React, { useState } from "react"

import Container from "react-bootstrap/Container"
import Row from "react-bootstrap/Row"
import Button from "react-bootstrap/Button"
import Table from "react-bootstrap/Table"

import CreateIcon from "@material-ui/icons/Create"
import ClearIcon from "@material-ui/icons/Clear"
import AddIcon from "@material-ui/icons/Add"

import { AddModal } from "./actions-components/AddModal"
import { EditModal } from "./actions-components/EditModal"

export const Actions = () => {
  const [showAddModal, setShowAddModal] = useState(false)
  const [showEditModal, setShowEditModal] = useState(false)
  const [selectedRow, setSelectedRow] = useState(0)

  const [mockActions, setMockActions] = useState([
    {
      id: 0,
      name: "Twitter Action",
      type: "Text",
      value: "Value"
    },
    {
      id: 1,
      name: "Twitter Action",
      type: "Text",
      value: "Value"
    },
    {
      id: 2,
      name: "Twitter Action",
      type: "Text",
      value: "Value"
    },
    {
      id: 3,
      name: "Twitter Action",
      type: "Text",
      value: "Value"
    },
    {
      id: 4,
      name: "Twitter Action",
      type: "Text",
      value: "Value"
    },
  ])

  const handleRemoveItem = (id: number) => {
    setMockActions(mockActions.filter(item => item.id !== id))
  }

  const handleEditItem = (id: number) => {
    setSelectedRow(id)
    setShowEditModal(true)
  }

  return (
    <Container fluid>
      <Row className="justify-content-center m-5">
        <AddModal
          show={showAddModal}
          hide={() => setShowAddModal(false)}
          setMockActions={setMockActions}
        />
        <Button variant="primary" className="btn-round mr-3 ml-3" onClick={() => setShowAddModal(true)}><AddIcon /></Button>
      </Row>
      <Row className="justify-content-center ml-5 mr-5 mt-5">
        <Table striped bordered className="text-center table-hover">
          <thead>
            <tr>
              <th className="align-middle">Id</th>
              <th className="align-middle">Name</th>
              <th className="align-middle">Type</th>
              <th className="align-middle">Value</th>
              <th className="align-middle w-25">Actions</th>
            </tr>
          </thead>
          <tbody>
            {
              mockActions.map((item) => (
                <tr key={item.id}>
                  <td className="align-middle">{item.id}</td>
                  <td className="align-middle">{item.name}</td>
                  <td className="align-middle">{item.type}</td>
                  <td className="align-middle">{item.value}</td>
                  <td className="align-middle">
                    <EditModal
                      show={showEditModal}
                      hide={() => setShowEditModal(false)}
                      setMockActions={setMockActions}
                      mockActions={mockActions}
                      selectedRow={selectedRow}
                    />
                    <Button className="btn-round mr-3 ml-3" onClick={() => handleEditItem(item.id)}><CreateIcon /></Button>
                    <Button className="btn-round mr-3 ml-3" onClick={() => handleRemoveItem(item.id)}><ClearIcon /></Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>
      </Row>
    </Container>
  )
}
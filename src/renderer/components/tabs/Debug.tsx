import React, { useState } from "react"

import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Form from "react-bootstrap/esm/Form"

export const Debug = () => {
  const [actionLog] = useState<string[]>([])

  const [mockData] = useState([
    {
      id: 0,
      name: "Keyboard 0",
      vid: 123,
      pid: 345,
      status: "status"
    },
    {
      id: 1,
      name: "Keyboard 1",
      vid: 123,
      pid: 345,
      status: "status"
    },
    {
      id: 2,
      name: "Keyboard 2",
      vid: 123,
      pid: 345,
      status: "status"
    },
    {
      id: 3,
      name: "Keyboard 3",
      vid: 123,
      pid: 345,
      status: "status"
    },
    {
      id: 4,
      name: "Keyboard 4",
      vid: 123,
      pid: 345,
      status: "status"
    }
  ])
  
  return (
    <>
      <Row>
        <Col xs={12} className={"text-center"}>
          <Form>
            <Form.Row className={"text-center justify-content-center"}>
              <Col xs={4}>
                <Form.Group controlId="example.SelectCustom">
                  <Form.Label><strong>Verfügbare Geräte:</strong></Form.Label>
                  <Form.Control as="select">
                    {
                      mockData.map((item) => (
                        <option key={item.id}>{item.name}</option>
                      ))
                    }
                  </Form.Control>
                </Form.Group>
              </Col>
            </Form.Row>
          </Form>
        </Col>
      </Row>
      <Row className="ml-5 mr-5">
        <textarea
          className="form-control"
          style={{
            resize: "none",
            height: 260,
          }}
          disabled
          value={actionLog.join("\n")} />
      </Row>
    </>
  )
}
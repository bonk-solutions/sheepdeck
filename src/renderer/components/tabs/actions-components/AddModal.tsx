import React, { ChangeEvent, Dispatch, FormEvent, useEffect, useState, SetStateAction } from "react"
import Col from "react-bootstrap/Col"
import Row from "react-bootstrap/Row"
import Button from "react-bootstrap/Button"
import Form from "react-bootstrap/Form"
import Modal from "react-bootstrap/esm/Modal"

interface MockActions {
  id: number;
  name: string;
  type: string;
  value: string;
}

interface Props {
  show: boolean
  hide: () => void
  setMockActions: Dispatch<SetStateAction<MockActions[]>>
}

export const AddModal = ({ show, hide, setMockActions }: Props) => {
  const [changeValue, setChangeValue] = useState("")

  useEffect(() => {
    if (show) {
      setChangeValue("")
    }
  }, [show])

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setChangeValue(event.target.value)
  }

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault()
    const newAction = {
      id: 0,
      name: changeValue,
      type: "",
      value: ""
    }
    
    setMockActions(oldActions => [...oldActions, newAction])
    hide()
  }

  return (
    <Modal centered show={show} onHide={hide}>
      <Modal.Header>
        <Modal.Title>Create Action</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Row sm={10}>
            <Form.Control
              className="ml-3 mr-3"
              onChange={handleChange}
              type="text"
            />
          </Row>
          <Row>
            <Col>
              <Button
                onClick={hide}
                variant="secondary"
              >
                Cancel
              </Button>
            </Col>
            <Col className="d-flex justify-content-end">
              <Button
                variant="primary"
                type="submit"
              >
                Submit
              </Button>
            </Col>
          </Row>

        </Form>
      </Modal.Body>
    </Modal>
  )
}
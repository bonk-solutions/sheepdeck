import React, { useState } from "react"
import "bootstrap/dist/css/bootstrap.min.css"
import Sheep from "../../assets/sheep.png"
import Container from "react-bootstrap/esm/Container"
import Row from "react-bootstrap/esm/Row"
import Button from "react-bootstrap/Button"
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos"
import Modal from "react-bootstrap/esm/Modal"
import { FoundKeyboards } from "./FoundKeyboards"

interface Props {
  hide: () => void
}

export const Welcome = ({ hide }: Props) => {
  const [showInstructions, setShowInstructions] = useState(false)

  return (
    <Container fluid className="h-100 d-flex flex-column justify-content-center align-items-center">

      <Modal
        centered
        show={showInstructions}
        onHide={() => setShowInstructions(false)}
      >
        <Modal.Header>
          <Modal.Title>Anleitung</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <ol>
            <li>Laden Sie Zadiq herunter https://zadig.akeo.ie/<br/>(kopieren mit Ctrl + C oder einfach online suchen)</li>
            <li>Klicken Sie Options {"->"} List All Devices</li>
            <li>
              Wählen Sie aus der Liste die <em>richtige Tastatur</em>, die sie patchen wollen
              <br/>Wenn Sie sich nicht sicher sind, stecken sie andere Geräte ab, um sicher zu stellen, dass sie das richtige Gerät patchen!
            </li>
            <li>Wählen rechts vom orangen Pfeil den Eintrag mit &quot;libusb&quot; aus</li>
            <li>Klicken Sie Replace Driver und folgen Sie den Anweisungen</li>
            <li>Falls SheepDeck schon läuft, starten sie das Programm neu</li>
            <li>Jetzt sollten sie ihre Tastatur mit SheepDeck benutzen können!</li>
          </ol>
        </Modal.Body>

        <Modal.Footer>
          <Button
            onClick={() => setShowInstructions(false)}
            variant="primary"
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>

      <Row>
        <h2>SheepDeck</h2>
      </Row>

      <Row>
        <img
          src={Sheep}
          style={{ height: 300 }}
        />
      </Row>

      <Row className="text-white">
        <h5>
          <FoundKeyboards/>
        </h5>
      </Row>

      <Row className="mt-4">
        <Button
          onClick={() => setShowInstructions(true)}
          variant="primary"
        >
          Anleitung
        </Button>
      </Row>

      <Row className="position-absolute" style={{
        right: "3rem",
        bottom: "3rem"
      }}>
        <Button
          onClick={hide}
          variant="primary"
          className="btn-round"
        >
          <ArrowForwardIosIcon />
        </Button>
      </Row>

    </Container>
  )
}
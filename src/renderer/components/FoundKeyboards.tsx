import React from "react"
import Badge from "react-bootstrap/Badge"
import { useDDs } from "@renderer/hooks/use-devices"

export const FoundKeyboards = () => {
  const dds = useDDs()

  return (
    <>
      Es wurden{" "}
      <Badge variant={"primary"}>
        {dds.length}
      </Badge>
      {" "}Geräte gefunden
    </>
  )
}
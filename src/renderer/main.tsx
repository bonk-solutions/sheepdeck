import React from "react"
import ReactDOM from "react-dom"
import { App } from "./components/App"

import "bootstrap/dist/css/bootstrap.min.css"
import "bootswatch/dist/cyborg/bootstrap.min.css"
import "./app.css"

const root = document.getElementById("root")
ReactDOM.render(<App/>, root)
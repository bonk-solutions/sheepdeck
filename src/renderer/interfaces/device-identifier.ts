export interface DeviceIdentifier {
  vendorId: number
  productId: number
}
import { DeviceDescriptor } from "usb"
import { isSameDD } from "../main/functions"

export const getKeyFromDD = (deviceDescriptor: DeviceDescriptor) => {
  return deviceDescriptor.idProduct + deviceDescriptor.idVendor
}

export const findDDIn = (searchDD: DeviceDescriptor) => (
  (iterationDD: DeviceDescriptor) => (
    isSameDD(searchDD, iterationDD)
  )
)

export const filterDDOut = (filteredOutDD: DeviceDescriptor) => (
  (iterationDD: DeviceDescriptor) => (
    !isSameDD(filteredOutDD, iterationDD)
  )
)
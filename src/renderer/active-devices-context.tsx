import { createContext } from "react";
import { Device } from "usb";

type ContextType = [
  Device[],
  (devices: Device[]) => void
];

export const ActiveDeviceContext = createContext<ContextType>([
  [],
  () => {}
]);
import { IpcRenderer } from "electron"
import { MainMsgs, MsgLookup, RendererMsgs } from "../ipc-messages"

// @ts-ignore
// eslint-disable-next-line @typescript-eslint/no-var-requires
const ipc = require("electron").ipcRenderer as IpcRenderer

export type ListenerFunction<T> = (
  event: Electron.IpcRendererEvent,
  payload: T
) => void

export const on = <Channel extends MainMsgs>(
  channel: Channel,
  listener: ListenerFunction<MsgLookup["main"][Channel]>
) => {
  ipc.on(channel, listener)
}

export const off = <Channel extends MainMsgs>(
  channel: Channel,
  listener: ListenerFunction<MsgLookup["main"][Channel]>
) => {
  ipc.off(channel, listener)
}

export const invoke = <Channel extends RendererMsgs>(
  channel: Channel,
  payload: any
): Promise<MsgLookup["renderer"][Channel]> => ipc.invoke(channel, payload)
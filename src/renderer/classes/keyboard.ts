export interface Keyboard {
  name: string
  vendorId: number
  productId: number
}
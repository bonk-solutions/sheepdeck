import { DeviceDescriptor } from "usb"

type WithDevice<T> = T & {
  deviceDescriptor: DeviceDescriptor
}

export interface MsgLookup {
  main: {
    "usb-data": WithDevice<{ data: Uint8Array }>
    "usb-error": WithDevice<{ error: any }>
    "devices-update": DeviceDescriptor[]
  }
  renderer: {
    "get-devices": DeviceDescriptor[]
    "activate-device": void
    "deactivate-device": void
  }
}

export type MainMsgs = keyof MsgLookup["main"]
export type RendererMsgs = keyof MsgLookup["renderer"]
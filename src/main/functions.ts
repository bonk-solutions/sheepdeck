import { Device, DeviceDescriptor, InEndpoint } from "usb"

export const isSameDD = (descA: DeviceDescriptor, descB: DeviceDescriptor): boolean => (
  descA.idVendor === descB.idVendor && descA.idProduct === descB.idProduct
)

export const isSameDevice = (deviceA: Device, deviceB: Device) => (
  isSameDD(
    deviceA.deviceDescriptor,
    deviceB.deviceDescriptor
  )
)

export const getEndpoint = (device: Device): InEndpoint|null => {
  try {
    const int = device.interface(0)
    return int.endpoints[0] as InEndpoint
  } catch {
    return null
  }
}
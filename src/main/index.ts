import { app, BrowserWindow } from "electron"
import path from "path"
import { pathToFileURL } from "url"

import { updateWebContents } from "./devices"

const isDevelopment = process.env.NODE_ENV === "development"

const createWindow = () => {
  const win = new BrowserWindow({
    width: 840,
    height: 845,
    minWidth:840,
    minHeight:845,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
    show: false,
  }).once("ready-to-show", () => {
    win.show()
  })
  if (isDevelopment) {
    win.loadURL("http://localhost:3000")
    win.webContents.toggleDevTools()
  } else {
    win.loadURL(
      pathToFileURL(path.join(__dirname, "./renderer/index.html")).toString()
    )
  }

  updateWebContents(win.webContents)
  win.on("close", () => updateWebContents(null))
}

app.whenReady().then(createWindow)

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit()
  }
})

app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

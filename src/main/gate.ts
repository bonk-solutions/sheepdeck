import { ipcMain, WebContents } from "electron"
import { MainMsgs, MsgLookup, RendererMsgs } from "../ipc-messages"
import { IpcMainInvokeEvent } from "electron/main"

let webContents: WebContents|null = null

export const updateWebContents = (newWebContents: WebContents|null) => {
  webContents = newWebContents
}

export const send = <Channel extends MainMsgs>(
  channel: Channel,
  payload: MsgLookup["main"][Channel]
) => {
  webContents?.send(channel, payload)
}

export const handle = <Channel extends RendererMsgs>(
  channel: Channel,
  listener: (
    event: IpcMainInvokeEvent,
    payload: any
  ) => void
) => {
  ipcMain.handle(channel, listener)
}
import { Device, DeviceDescriptor } from "usb"
import { isSameDD, getEndpoint, isSameDevice } from "./functions"
import { send, handle, updateWebContents as up } from "./gate"

// eslint-disable-next-line @typescript-eslint/no-var-requires
const usb = require("usb") as typeof import("usb")

export const updateWebContents = up

let activeDevices: Device[] = []

const getDevice = (desc: DeviceDescriptor): Device|undefined => (
  usb.getDeviceList().find(d => isSameDD(d.deviceDescriptor, desc))
)

const getDDs = () => usb
  .getDeviceList()
  .map(d => d.deviceDescriptor)
  .filter(dd => dd.bDescriptorType === 1) // nur Tastaturen / HID

const dataHandler = (data: Buffer, { deviceDescriptor }: Device) => {
  console.log("sending usb-data...")
  send("usb-data", { data, deviceDescriptor })
}

const errorHandler = (error: Error, { deviceDescriptor }: Device) => {
  console.error("usb error", error)
  send("usb-error", { error, deviceDescriptor })
}

const deactivate = (device: Device) => new Promise<void>(res => {
  if (!device.interfaces) {
    res()
    return
  }

  const endpoint = getEndpoint(device)
  
  if (!endpoint) {
    return
  }

  const cleanup = () => {
    endpoint.removeAllListeners()
    device.close()
    console.info("device closed")
    res()
  }

  // @ts-ignore
  if (!endpoint.pollActive) { // doch, gibt es
    cleanup()
    return
  }

  endpoint.stopPoll()
  endpoint.once("end", cleanup)
})

const activate = (device: Device) => {
  device.open()
  console.log("device opened")

  if (!device.interfaces) {
    console.log("no interface", device.deviceDescriptor, device.interfaces)
    return
  }

  device.interfaces[0].claim()

  const endpoint = getEndpoint(device)
  
  if (!endpoint) {
    throw new Error("no endpoint")
  }

  endpoint.startPoll()

  endpoint.addListener("data", data => {
    dataHandler(data, device)
  })

  endpoint.addListener("error", error => {
    errorHandler(error, device)
  })
}

const refresh = () => {
  const dds = getDDs()
  console.log(`Refreshing... [${dds.length}]`)
  send("devices-update", dds)
}

usb.on("attach", refresh)
usb.on("detach", refresh)

refresh()

handle("get-devices", getDDs)

handle("activate-device", (_, desc) => {
  const device = getDevice(desc)

  if (!device) {
    return
  }
  
  activeDevices = [...activeDevices, device]
  activate(device)
})

handle("deactivate-device", (_, desc) => {
  const device = getDevice(desc)

  if (!device) {
    return
  }

  activeDevices = activeDevices.filter(d => !isSameDevice(d, device))
  deactivate(device)
})
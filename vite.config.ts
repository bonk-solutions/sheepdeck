import { defineConfig } from "vite"
import reactRefresh from "@vitejs/plugin-react-refresh"
import path from "path"

const rendererPath = path.resolve(__dirname, "./src/renderer")
const outDirRenderer = path.resolve(__dirname, "./app/renderer")

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [reactRefresh()],
  base: "./",
  root: rendererPath,
  build: {
    outDir: outDirRenderer,
    emptyOutDir: true,
    lib: {
      formats: ["cjs"],
      entry: "src/main/index.ts"
    }
  },
  resolve: {
    alias: [
      {
        find: "@renderer",
        replacement: path.resolve(__dirname, "src/renderer"),
      },
      {
        find: "@common",
        replacement: path.resolve(__dirname, "src/common"),
      },
    ],
  },
})

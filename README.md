# SheepDeck
Der next-gen Keyboard Mapper für Streamer.

### Was ist SheepDeck?
SheepDeck ist eine Desktop Anwendung, die es erlaubt, eine beliebige USB Tastatur zur Steuerung verschiedener anderer Anwendung und Dienste zu verwenden. Diese Dienste drehen sich hauptsächlich um das Streamen (z.B. auf Twitch oder YouTube), aber das SheepDeck kann durch zahlreiche eingebaute Plugins auch für alltägliche Arbeit oder beim Gaming benutzt werden.

### Was macht SheepDeck besonders?
SheepDeck ist an das Stream Deck von Elgato angelehnt, kostet aber anstelle der ~100€ für ein normales Stream Deck nur einen Bruchteil, in dem man eine beliebige USB Tastatur benutzen kann.

SheepDeck unterscheidet sich von anderen, ähnlichen Lösungen, die mit USB Tastaturen arbeiten, in dem es das patchen des Treibers für die Tastatur erfordert. Andere Lösungen basieren auf Hotkey Kombinationen oder Ähnlichem und funktionieren unter Umständen nicht innerhalb von manchen Applikationen und sind generell unzuverlässiger als ein Stream Deck. Mit dem Patchen des Treibers umgeht SheepDeck dieses Problem und kann direkt über die Treiber-Schnittstelle die Rohdaten der Tastatur lesen und den User Aktionen zuweisen lassen.

Mit diesem Prinzip kann SheepDeck die Verlässigkeit eines Stream Decks und die niedrigen (bis keine) Kosten einer einfachen USB Tastatur oder Numpad verbinden.

### Installation

- Erfordert Node.js (kommt mit `npm`)
- Installieren der Pakete: `npm install`
- Starten der Anwendung: `npm run dev`
- Weitere Scripts stehen in der `package.json`
import React from 'react'
import { render } from 'react-dom'

import 'bootstrap/dist/css/bootstrap.min.css'
import Button from 'react-bootstrap/Button'

const mainElement = document.createElement('div')
mainElement.setAttribute('id', 'root')
document.body.appendChild(mainElement)

const App = () => {
  const [counter, setCounter] = React.useState(0)
  const [signalLog, setSignalLog] = React.useState<string[]>([])
  const [actionLog, setActionLog] = React.useState<string[]>([])

  React.useEffect(() => {
    setInterval(() => {
      setCounter(oldCounter => oldCounter + 1)
    }, 2000)
  }, [])

  React.useEffect(() => {
    const lastFive = signalLog.slice(-9)
    setSignalLog([...lastFive, String(counter)])
  }, [counter])

  React.useEffect(() => {
    const lastFive = actionLog.slice(-9)
    setActionLog([...lastFive, String(counter)])
  }, [counter])

  return (
    <div
      style={{
        display: 'flex'
      }}>
      <div
        style={{
          margin: 25
        }}>
        <p>Signals:</p>
        <textarea
          disabled
          style={{
            resize: 'none',
            width: 500,
            height: 300
          }}
          value={signalLog.join('\n')}
        >
        </textarea>
      </div>

      <div
        style={{
          margin: 25
        }}>
        <p>Actions:</p>
        <textarea
          disabled
          style={{
            resize: 'none',
            width: 500,
            height: 300
          }}
          value={actionLog.join('\n')}
        >
        </textarea>
      </div>

    </div>
  )
}

render(<App />, mainElement)

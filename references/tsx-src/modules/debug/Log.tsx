import * as React from "react";
import { ReactElement } from "react";

interface Props {
  name: string
  log: string[]
}

export const Log = ({ name, log }: Props): ReactElement => {
  const trimmedLog = log.splice(-10);

  return (
    <div>
      <p>{name}</p>
      <textarea
        disabled
        style={{ marginTop: "1rem" }}
      >
        {trimmedLog.join("\n")}
      </textarea>
    </div>
  );
};

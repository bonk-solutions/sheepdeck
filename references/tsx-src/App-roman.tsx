import React, { ReactElement } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import { Log } from "./modules/debug/Log";

export const App = (): ReactElement => {
  return (
    <Tabs defaultActiveKey="debug" id="uncontrolled-tab-example">
      <Tab eventKey="devices" title="Devices" disabled></Tab>
      <Tab eventKey="mappers" title="Mappers" disabled></Tab>
      <Tab eventKey="services" title="Services" disabled></Tab>
      <Tab eventKey="debug" title="Debug">
        <Log name="Signals" log={[""]}/>
      </Tab>
    </Tabs>
  );
};

export interface SignalKeyBinding {
  signal: string
  key: string
}

export interface KeyMapping {
  name: string
  author: string
  bindings: SignalKeyBinding[]
}
import { AbstractWrapper } from "./AbstractWrapper";
import { ActionMapping, KeyActionBinding } from "./ActionMapping";

export class ActionMappingWrapper extends AbstractWrapper<ActionMapping, KeyActionBinding, () => void> {
  addBinding(binding: KeyActionBinding) {
    if (!this.mapping) {
      throw new Error("ActionMapping nicht definiert");
    }

    this.mapping.bindings = [...this.mapping.bindings, binding];
  }

  removeBinding(key: string) {
    if (!this.mapping) {
      throw new Error("ActionMapping nicht definiert");
    }

    this.mapping.bindings = this.mapping.bindings
      .filter((binding) => binding.key !== key);
  }

  editBinding(key: string, newAction: () => void) {
    if (!this.mapping) {
      throw new Error("ActionMapping nicht definiert");
    }

    const maybeBinding = this.mapping.bindings
      .find((binding) => binding.key === key);

    if (!maybeBinding) {
      throw new Error("Key wurde im Binding nicht gefunden");
    }

    maybeBinding.action = newAction;
  }
}
export interface KeyActionBinding {
  key: string
  action: Function
}

export interface ActionMapping {
  name: string
  author: string
  bindings: KeyActionBinding[]
}

// const foo2: ActionMapping = {
//   name: "test",
//   author: "test",
//   bindings: [
//     { action: () => {}, key: "Enter" }
//   ]
// }

// function main() {
//   const incomingSignal = "10";
  
//   const maybeSignalKey = foo.bindings.find(signalKey => signalKey.signal === incomingSignal);
  
//   if (maybeSignalKey === undefined) {
//     return;
//   }

//   const incomingKey = maybeSignalKey.key;

//   const maybeKeyAction = foo2.bindings.find(keyAction => keyAction.key === incomingKey);

//   if (maybeKeyAction === undefined) {
//     return;
//   }

//   const incomingAction = maybeKeyAction.action;

//   incomingAction();
// }

// main();
import { readFile, writeFile } from "fs/promises";
import * as path from "path";

export abstract class AbstractWrapper<T, U, V> {
  protected mapping?: T;

  async load(path: string): Promise<void> {
    try {
      const data = await readFile(path, 'utf-8');

      if (!data) {
        throw new Error("Daten sind leer!");
      }

      // Type override parsed data
      this.mapping = JSON.parse(data) as T;
    } catch (error) {
      throw new Error("Fehler beim Laden vom KeyMapping");
    }
  }

  abstract addBinding(binding: U): void

  abstract removeBinding(descriptor: string): void

  abstract editBinding(descriptor: string, mappingTarget: V): void

  async save(fileName: string, filePath: string) {
    const data = JSON.stringify(this.mapping);

    await writeFile(path.join(filePath, fileName), data);

    console.log("Data written to file " + fileName);
  }
}
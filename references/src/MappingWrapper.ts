import { KeyMapping, SignalKeyBinding } from "./KeyMapping";
import { AbstractWrapper } from "./AbstractWrapper";

export class KeyMappingWrapper extends AbstractWrapper<KeyMapping, SignalKeyBinding, string> {
  addBinding(binding: SignalKeyBinding) {
    if (!this.mapping) {
      throw new Error("KeyMapping nicht definiert");
    }

    this.mapping.bindings = [...this.mapping.bindings, binding];
  }

  removeBinding(signal: string) {
    if (!this.mapping) {
      throw new Error("KeyMapping nicht definiert");
    }

    this.mapping.bindings = this.mapping.bindings
      .filter((binding) => binding.signal !== signal);
  }

  editBinding(signal: string, newKey: string) {
    if (!this.mapping) {
      throw new Error("KeyMapping nicht definiert");
    }

    const maybeBinding = this.mapping.bindings
      .find((binding) => binding.signal === signal);

    if (!maybeBinding) {
      throw new Error("Key wurde im Binding nicht gefunden");
    }

    maybeBinding.key = newKey;
  }
}
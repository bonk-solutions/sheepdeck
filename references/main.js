const usb = require("usb");

const productId = 720;
const vendorId = 3163;

const maybeDevice = usb.findByIds(vendorId, productId);

if (maybeDevice === undefined) {
  throw new Error("Gerät konnte nicht gefunden werden!");
}

maybeDevice.open();

const keyboardInterface = maybeDevice.interface(0);

keyboardInterface.claim();

const endpoint = keyboardInterface.endpoints[0];

if (!endpoint) {
  throw new Error("Konnte endpoint nicht öffnen!");
}

let lastSignalWasZeros = false;

function listen() {
  endpoint.transfer(8, (err, buffer) => {
    if (err) {
      throw err;
    }

    const data = buffer.toJSON().data;

    const signalIsZeros = data.every(byte => byte === 0);

    if (lastSignalWasZeros && signalIsZeros) {
      listen();
      return;
    }

    lastSignalWasZeros = signalIsZeros;

    console.log(data);

    listen();
  })
}

listen();